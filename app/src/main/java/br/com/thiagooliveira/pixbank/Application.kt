package br.com.thiagooliveira.pixbank

import android.app.Application
import br.com.thiagooliveira.pixbank.utils.KoinUtils

class Application : Application() {
    override fun onCreate() {
        super.onCreate()

        KoinUtils.createInstance(this)
    }
}