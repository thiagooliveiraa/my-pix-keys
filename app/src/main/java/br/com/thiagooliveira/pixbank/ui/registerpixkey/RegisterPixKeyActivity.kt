package br.com.thiagooliveira.pixbank.ui.registerpixkey

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import br.com.thiagooliveira.domain.enum.PixKeyType
import br.com.thiagooliveira.domain.model.PixKey
import br.com.thiagooliveira.pixbank.R
import br.com.thiagooliveira.pixbank.databinding.ActivityRegisterPixKeyBinding
import br.com.thiagooliveira.pixbank.extensions.nonNullObserve
import br.com.thiagooliveira.pixbank.extensions.setVisible
import br.com.thiagooliveira.pixbank.ui.registerpixkey.selectkeytype.SelectKeyTypeBottomSheet
import br.com.thiagooliveira.pixbank.utils.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegisterPixKeyActivity : AppCompatActivity(),
    SelectKeyTypeBottomSheet.SelectKeyTypeBottomSheetContract {

    private val pixkey: PixKey = PixKey()
    private val registerPixKeyViewModel by viewModel<RegisterPixKeyViewModel>()

    private lateinit var binding: ActivityRegisterPixKeyBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterPixKeyBinding.inflate(layoutInflater)

        setupViews()
        observerRegisterPixKey()

        setContentView(binding.root)
    }

    private fun setupViews() {
        setupToolbar()
        setupSelectKeyType()
        setupRegisterPixKeyButton()
    }

    private fun observerRegisterPixKey() {
        registerPixKeyViewModel.registerPixKeyState.nonNullObserve(this) { registerPixKeyState ->
            when (registerPixKeyState) {
                is StateLoading -> {
                    showLoading(isLoading = true)
                }
                is StateSuccess -> {
                    showLoading(isLoading = false)
                    intent.putExtra(PIX_KEY, registerPixKeyState.data)
                    setResult(NEW_PIX_KEY_REQUEST, intent)
                    finish()
                }
                is StateError -> {
                    showLoading(isLoading = false)
                    Toast.makeText(this, R.string.error_on_register_pix_key, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun setupToolbar() {
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            supportActionBar?.elevation = 0f
            title = getString(R.string.register_pix_key)
        }
    }

    private fun setupSelectKeyType() {
        binding.includeSelectPixKeyType.apply {
            pixkey.type?.let {
                val typeUi = PixKeyTypeUi.valueOf(it.name)
                registerSelectIcon.setImageResource(typeUi.icon)
                registerSelectTitle.apply {
                    text = typeUi.description
                    setTextColor(ContextCompat.getColor(this@RegisterPixKeyActivity, R.color.colorBlack))
                }
            } ?: run {
                registerSelectIcon.apply {
                    setImageResource(R.drawable.ic_format_list_bulleted_type)
                    setColorFilter(
                        ContextCompat.getColor(this@RegisterPixKeyActivity, R.color.colorBlackLigth),
                        android.graphics.PorterDuff.Mode.SRC_IN
                    )
                }

                registerSelectTitle.apply {
                    text = getString(R.string.select_key_type_title)
                    registerSelectTitle.setTextColor(ContextCompat.getColor(this@RegisterPixKeyActivity, R.color.colorBlackLigth))
                }
            }
            root.setOnClickListener {
                openSelectKeyTypeDialog()
            }
        }
    }

    private fun setupRegisterPixKeyButton() {
        binding.btRegisterMyKey.setOnClickListener {
            startRegisterPixKey()
        }
    }

    private fun openSelectKeyTypeDialog() {
        val dialog = SelectKeyTypeBottomSheet.newInstance(
            pixKeyTypeSelected = pixkey.type?.name?.let { PixKeyTypeUi.valueOf(it) }
        )
        dialog.show(supportFragmentManager, SelectKeyTypeBottomSheet.TAG)
    }

    private fun startRegisterPixKey() {
        if (validFields()) {
            registerPixKeyViewModel.registerPixKey(pixkey)
        } else {
            Toast.makeText(
                this,
                getString(R.string.register_key_validation_message),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun validFields(): Boolean {
        val registerInputEdt = binding.includeInputKeyValue.registerInputEdt
        pixkey.value = registerInputEdt.text.toString()
        return !pixkey.value.isNullOrBlank() && pixkey.type != null
    }

    private fun showLoading(isLoading: Boolean) {
        binding.btRegisterMyKey.setVisible(!isLoading)
        binding.progressRegisterPixKey.setVisible(isLoading)
    }

    override fun onSelectKeyType(keyType: PixKeyTypeUi) {
        pixkey.type = PixKeyType.valueOf(keyType.name)
        setupSelectKeyType()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val NEW_PIX_KEY_REQUEST = 1234
        const val PIX_KEY = "pix_key"

        fun newIntent(
            context: Context
        ): Intent {
            return Intent(context, RegisterPixKeyActivity::class.java)
        }
    }

}