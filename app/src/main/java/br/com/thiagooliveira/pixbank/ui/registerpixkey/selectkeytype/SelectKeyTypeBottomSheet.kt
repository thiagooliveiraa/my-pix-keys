package br.com.thiagooliveira.pixbank.ui.registerpixkey.selectkeytype

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.thiagooliveira.pixbank.databinding.SelectKeyTypeBottomSheetDialogBinding
import br.com.thiagooliveira.pixbank.utils.PixKeyTypeUi
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class SelectKeyTypeBottomSheet : BottomSheetDialogFragment() {

    private var pixKeyTypeSelected: PixKeyTypeUi? = null
    private var listener: SelectKeyTypeBottomSheetContract? = null

    private lateinit var binding: SelectKeyTypeBottomSheetDialogBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = SelectKeyTypeBottomSheetDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onAttach(context: Context) {
        listener = context as? SelectKeyTypeBottomSheetContract
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupCancelButton()
        setupKeyTypesList()
    }

    private fun setupCancelButton() {
        binding.btCancelSelectKeyTypeDialog.setOnClickListener {
            dialog?.dismiss()
        }
    }

    private fun setupKeyTypesList() {
        pixKeyTypeSelected = arguments?.get(PIX_TYPE_SELECTED) as? PixKeyTypeUi

        val keyTypes = PixKeyTypeUi.values().toList()
        binding.keyTypesRv.adapter = KeyTypesAdapter(
            keyTypes = keyTypes,
            keyTypeSelected = pixKeyTypeSelected
        ) {
            dismiss()
            listener?.onSelectKeyType(it)
        }
    }

    interface SelectKeyTypeBottomSheetContract {
        fun onSelectKeyType(keyType: PixKeyTypeUi)
    }

    companion object {

        const val PIX_TYPE_SELECTED = "PIX_TYPE_SELECTED"

        fun newInstance(
            pixKeyTypeSelected: PixKeyTypeUi? = null
        ) : SelectKeyTypeBottomSheet {
            val arguments = Bundle().apply {
                this.putSerializable(PIX_TYPE_SELECTED, pixKeyTypeSelected)
            }
            return SelectKeyTypeBottomSheet().apply {
                this.arguments = arguments
            }
        }

        const val TAG = "SelectKeyTypeBottomSheet"
    }

}