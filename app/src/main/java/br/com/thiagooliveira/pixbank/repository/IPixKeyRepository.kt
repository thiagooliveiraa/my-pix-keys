package br.com.thiagooliveira.pixbank.repository

import br.com.thiagooliveira.domain.model.PixKey
import br.com.thiagooliveira.pixbank.utils.Result

interface IPixKeyRepository {
    suspend fun getMyPixKeys(): Result<List<PixKey>>
    suspend fun registerPixKey(pixKey: PixKey): Result<PixKey>
}