package br.com.thiagooliveira.pixbank.ui.mypixkeyslist

import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.thiagooliveira.data.di.DataDependencyInject
import br.com.thiagooliveira.domain.model.PixKey
import br.com.thiagooliveira.pixbank.databinding.ActivityMainBinding
import br.com.thiagooliveira.pixbank.di.UiDependencyInject
import br.com.thiagooliveira.pixbank.extensions.nonNullObserve
import br.com.thiagooliveira.pixbank.extensions.setVisible
import br.com.thiagooliveira.pixbank.ui.mypixkeysdetails.MyPixKeyDetailsActivity
import br.com.thiagooliveira.pixbank.ui.registerpixkey.RegisterPixKeyActivity
import br.com.thiagooliveira.pixbank.utils.KoinUtils
import br.com.thiagooliveira.pixbank.utils.StateError
import br.com.thiagooliveira.pixbank.utils.StateLoading
import br.com.thiagooliveira.pixbank.utils.StateSuccess
import org.koin.androidx.viewmodel.ext.android.viewModel

class MyPixKeysActivity : AppCompatActivity() {

    private val myPixKeysViewModel by viewModel<MyPixKeysViewModel>()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        initKoin()
        setupBtGoToNewPixKeyScreen()
        observerMyPixKeys()

        myPixKeysViewModel.getMyPixKeys()

        setContentView(binding.root)
    }

    private fun setupBtGoToNewPixKeyScreen() {
        binding.btGoToNewPixKeyScreen.setOnClickListener {
            goToNewPixKeyScreen()
        }
    }

    private fun goToNewPixKeyScreen() {
        registerForActivityResult.launch(RegisterPixKeyActivity.newIntent(this))
    }

    private fun observerMyPixKeys() {
        myPixKeysViewModel.myKeysState.nonNullObserve(this) { myKeysState ->
            when (myKeysState) {
                is StateLoading -> {
                    binding.progressMain.setVisible(true)
                }
                is StateSuccess -> {
                    binding.progressMain.setVisible(false)
                    showMyPixKeysList(myKeysState.data)
                }
                is StateError -> {
                    binding.progressMain.setVisible(false)
                }
            }
        }
    }

    private fun showMyPixKeysList(myPixKeys: List<PixKey>) {
        if (myPixKeys.isNotEmpty()) {
            binding.myKeysRecyclerView.apply {
                layoutManager = LinearLayoutManager(this@MyPixKeysActivity)
                adapter = MyPixKeysAdapter(myPixKeys) { pixKey ->
                    MyPixKeyDetailsActivity.startActivity(this@MyPixKeysActivity, pixKey)
                }
                setVisible(true)
            }
        } else {
            binding.emptyMyPixKeysListLayout.root.setVisible(true)
        }
    }

    private fun initKoin() {
        KoinUtils.addModules(DataDependencyInject.remoteDataModule)
        KoinUtils.addModules(*UiDependencyInject.moduleList)
    }

    private val registerForActivityResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            val pixKeyRegistered = result.data?.getSerializableExtra(RegisterPixKeyActivity.PIX_KEY) as? PixKey
            pixKeyRegistered?.let { pixKey ->
                myPixKeysViewModel.updateMyPixKeys(pixKey)
            }
        }

    override fun onDestroy() {
        super.onDestroy()
        KoinUtils.removeModules(DataDependencyInject.remoteDataModule)
        KoinUtils.removeModules(*UiDependencyInject.moduleList)
    }
}
