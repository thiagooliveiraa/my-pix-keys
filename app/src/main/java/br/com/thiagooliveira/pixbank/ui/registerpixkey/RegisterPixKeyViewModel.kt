package br.com.thiagooliveira.pixbank.ui.registerpixkey

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.thiagooliveira.domain.model.PixKey
import br.com.thiagooliveira.pixbank.repository.IPixKeyRepository
import br.com.thiagooliveira.pixbank.utils.*
import kotlinx.coroutines.launch

class RegisterPixKeyViewModel(
    private val pixKeyRepository: IPixKeyRepository
) : BaseViewModel() {

    private val _registerPixKeyState = MutableLiveData<StateResponse<PixKey>>()
    val registerPixKeyState: LiveData<StateResponse<PixKey>> = _registerPixKeyState

    fun registerPixKey(pixKey: PixKey) {
        _registerPixKeyState.value = StateLoading()
        launch {
            pixKeyRepository.registerPixKey(pixKey)
                .onSuccess {
                    _registerPixKeyState.value = StateSuccess(it)
                }.onError {
                    _registerPixKeyState.value = StateError()
                }
        }
    }

}
