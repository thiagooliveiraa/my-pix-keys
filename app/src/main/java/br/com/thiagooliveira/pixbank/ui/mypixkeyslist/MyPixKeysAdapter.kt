package br.com.thiagooliveira.pixbank.ui.mypixkeyslist

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.thiagooliveira.domain.model.PixKey

class MyPixKeysAdapter(
    private val myPixKeys: List<PixKey>,
    private val onPixkeySelected: ((pixKey: PixKey) -> Unit)
) : RecyclerView.Adapter<MyPixKeyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyPixKeyViewHolder {
        return MyPixKeyViewHolder.create(parent, onPixkeySelected)
    }

    override fun onBindViewHolder(holder: MyPixKeyViewHolder, position: Int) {
        holder.bind(myPixKeys[position])
    }

    override fun getItemCount() = myPixKeys.size

}