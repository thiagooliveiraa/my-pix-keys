package br.com.thiagooliveira.pixbank.utils

sealed class Result<out T : Any>
class Success<out T : Any>(val data: T) : Result<T>()
open class Error(val message: String) : Result<Nothing>()

inline fun <T : Any> Result<T>.onSuccess(action: (T) -> Unit): Result<T> {
    if (this is Success) action(data)
    return this
}
inline fun <T : Any> Result<T>.onError(action: (Error) -> Unit): Result<T> {
    if (this is Error) action(this)
    return this
}