package br.com.thiagooliveira.pixbank.utils

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

fun coppyToClipBoard(
    label: String,
    text: String,
    message: Int,
    context: Context
) {
    val clipboard: ClipboardManager =
        context.getSystemService(AppCompatActivity.CLIPBOARD_SERVICE) as ClipboardManager
    val clip = ClipData.newPlainText(label, text)
    clipboard.setPrimaryClip(clip)

    Toast.makeText(context, context.getString(message), Toast.LENGTH_SHORT)
        .show()
}