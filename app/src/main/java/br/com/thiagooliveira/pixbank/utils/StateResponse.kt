package br.com.thiagooliveira.pixbank.utils

sealed class StateResponse<T>

class StateSuccess<T>(val data: T) : StateResponse<T>()
class StateError<T> (val errorData: Error? = null): StateResponse<T>()
class StateLoading<T> : StateResponse<T>()

fun <T> StateResponse<T>.data(): T? {
    return if (this is StateSuccess<T>) this.data else null
}
