package br.com.thiagooliveira.pixbank.utils

import br.com.thiagooliveira.pixbank.R

enum class PixKeyTypeUi(val description: String, val icon: Int) {
    CPF(
        description = "CPF",
        icon = R.drawable.ic_card_account_details_outline
    ),
    EMAIL(
        description = "E-mail",
       icon = R.drawable.ic_email_outline
    ),
    PHONE(
        description = "Celular",
        icon = R.drawable.ic_cellphone_android
    ),
    RANDOM(
        description = "Chave Aleatória",
        icon = R.drawable.ic_pencil_box_multiple_outline
    );

}