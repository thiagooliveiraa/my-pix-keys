package br.com.thiagooliveira.pixbank.ui.registerpixkey.selectkeytype

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.thiagooliveira.pixbank.utils.PixKeyTypeUi

class KeyTypesAdapter(
    private val keyTypes: List<PixKeyTypeUi>,
    private val keyTypeSelected: PixKeyTypeUi? = null,
    private val onKeyTypeSelected: (keyType: PixKeyTypeUi) -> Unit
) : RecyclerView.Adapter<KeyTypesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KeyTypesViewHolder {
        return KeyTypesViewHolder.create(parent, onKeyTypeSelected)
    }

    override fun onBindViewHolder(holder: KeyTypesViewHolder, position: Int) {
        val keyType = keyTypes[position]
        holder.bind(keyType, isSelected(keyType))
    }

    override fun getItemCount() = keyTypes.size

    private fun isSelected(keyType: PixKeyTypeUi): Boolean {
        return keyTypeSelected?.name == keyType.name
    }

}
