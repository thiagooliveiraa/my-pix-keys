package br.com.thiagooliveira.pixbank.repository

import br.com.thiagooliveira.data.provider.IPixKeysProvider
import br.com.thiagooliveira.data.retrofitutils.RequestResponse
import br.com.thiagooliveira.domain.model.PixKey
import br.com.thiagooliveira.pixbank.mapper.asDomainModel
import br.com.thiagooliveira.pixbank.utils.Error
import br.com.thiagooliveira.pixbank.utils.Result
import br.com.thiagooliveira.pixbank.utils.Success
import java.util.UUID

class PixKeyRepositoryImpl(
    private val pixKeysProvider: IPixKeysProvider
) : IPixKeyRepository {

    override suspend fun getMyPixKeys(): Result<List<PixKey>> {
        return when (val response = pixKeysProvider.getMyPixKeys()) {
            is RequestResponse.Error -> {
                Error(ERROR)
            }
            is RequestResponse.Success -> response.value?.map { it.asDomainModel() }?.let {
                Success(it)
            } ?: Error(ERROR)
        }
    }

    override suspend fun registerPixKey(pixKey: PixKey): Result<PixKey> {
        return when (pixKeysProvider.registerPixKey()) {
            is RequestResponse.Error -> {
                Error(ERROR)
            }
            is RequestResponse.Success -> {
                Success(pixKey.copy(
                    id = UUID.randomUUID().toString(),
                ))
            }
        }
    }

    companion object {
        private const val ERROR = "Error"
    }

}
