package br.com.thiagooliveira.pixbank.ui.registerpixkey.selectkeytype

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.thiagooliveira.pixbank.databinding.KeyTypeListItemBinding
import br.com.thiagooliveira.pixbank.extensions.setVisible
import br.com.thiagooliveira.pixbank.utils.PixKeyTypeUi

class KeyTypesViewHolder(
    private val itemViewBinding: KeyTypeListItemBinding,
    private val onKeyTypeSelected: (keyType: PixKeyTypeUi) -> Unit
) : RecyclerView.ViewHolder(itemViewBinding.root) {

    fun bind(keyType: PixKeyTypeUi, selected: Boolean) {
        itemViewBinding.titleKeyTypeListItem.text = keyType.description
        itemViewBinding.imgKeyTypeListItem.setImageResource(keyType.icon)
        itemViewBinding.checkKeyTypeSelected.setVisible(selected)

        itemView.setOnClickListener {
            onKeyTypeSelected.invoke(keyType)
        }
    }

    companion object {
        fun create(
            parent: ViewGroup,
            onKeyTypeSelected: (keyType: PixKeyTypeUi) -> Unit
        ): KeyTypesViewHolder {
            val itemViewBinding =
                KeyTypeListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return KeyTypesViewHolder(itemViewBinding, onKeyTypeSelected)
        }
    }

}
