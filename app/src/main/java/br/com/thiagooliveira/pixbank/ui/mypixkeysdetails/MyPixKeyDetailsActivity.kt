package br.com.thiagooliveira.pixbank.ui.mypixkeysdetails

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import br.com.thiagooliveira.domain.model.PixKey
import br.com.thiagooliveira.pixbank.R
import br.com.thiagooliveira.pixbank.databinding.ActivityMyPixKeyDetailsBinding
import br.com.thiagooliveira.pixbank.utils.PixKeyTypeUi
import br.com.thiagooliveira.pixbank.utils.coppyToClipBoard
import br.com.thiagooliveira.pixbank.utils.generateQRCode
import br.com.thiagooliveira.pixbank.utils.sharePlainText
import com.bumptech.glide.Glide

class MyPixKeyDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMyPixKeyDetailsBinding
    private var pixKey: PixKey? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMyPixKeyDetailsBinding.inflate(layoutInflater)

        setupToolbar()
        setupPixKey()
        setupViews()

        setContentView(binding.root)
    }

    private fun setupPixKey() {
        pixKey = intent.getSerializableExtra(INTENT_PIX_KEY) as? PixKey
    }

    private fun setupToolbar() {
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            title = getString(R.string.pix_key)
        }
    }

    private fun setupViews() {
        pixKey?.apply {
            setupPixKeyBankImage(this)
            setupPixKeyType(this)
            value?.let { pixKeyValue ->
                setupPixKeyValue(pixKeyValue)
                setupPixKeyQRCode(pixKeyValue)
                setupShareKeyButton(pixKeyValue)
                setupCopyKeyButton(pixKeyValue)
            }
        }
    }

    private fun setupPixKeyBankImage(pixKey: PixKey) {
        pixKey.bankImage?.let { banckImg ->
            binding.imgPixKeyDetails.apply {
                Glide.with(context)
                    .load(banckImg)
                    .circleCrop()
                    .into(this)
            }
        }
    }

    private fun setupPixKeyType(pixKey: PixKey) {
        pixKey.type?.let { pixKeyType ->
            binding.pixKeyTypePixKeyDetails.text = PixKeyTypeUi.valueOf(pixKeyType.name).description
        }
    }

    private fun setupPixKeyValue(value: String) {
        binding.pixKeyValuePixKeyDetails.text = value
    }

    private fun setupPixKeyQRCode(value: String) {
        binding.qrcodePixKeyDetails.let { imageView ->
            generateQRCode(imageView, value)
        }
    }

    private fun setupShareKeyButton(value: String) {
        binding.btSharePixKeyDetails.setOnClickListener {
            sharePlainText(this, value)
        }
    }

    private fun setupCopyKeyButton(value: String) {
        binding.btCopyPixKeyDetails.setOnClickListener {
            coppyToClipBoard(getString(R.string.copy_key), value, R.string.copy_to_clipboard, this)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {

        const val INTENT_PIX_KEY = "INTENT_PIX_KEY"

        fun startActivity(
            context: Context,
            pixKey: PixKey
        ) {
            val intent = Intent(context, MyPixKeyDetailsActivity::class.java)
            intent.putExtra(INTENT_PIX_KEY, pixKey)
            (context as Activity).startActivity(intent)
        }
    }
}