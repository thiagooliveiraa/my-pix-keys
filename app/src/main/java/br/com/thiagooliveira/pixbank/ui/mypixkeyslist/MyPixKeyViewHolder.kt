package br.com.thiagooliveira.pixbank.ui.mypixkeyslist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.thiagooliveira.domain.model.PixKey
import br.com.thiagooliveira.pixbank.databinding.MyPixKeyItemBinding
import br.com.thiagooliveira.pixbank.utils.PixKeyTypeUi
import com.bumptech.glide.Glide

class MyPixKeyViewHolder(
    private val itemViewBinding: MyPixKeyItemBinding,
    private val onPixKeySelected: (pixKey: PixKey) -> Unit
) : RecyclerView.ViewHolder(itemViewBinding.root) {

    fun bind(pixKey: PixKey) {
        itemViewBinding.bankImageMyPixKeyItem.apply {
            Glide.with(context)
                .load(pixKey.bankImage)
                .circleCrop()
                .into(this)
        }
        itemViewBinding.typeMyPixKeyItem.text = pixKey.type?.name?.let {
            PixKeyTypeUi.valueOf(it).description
        }
        itemViewBinding.valueMyPixKeyItem.text = pixKey.value

        itemView.setOnClickListener {
            onPixKeySelected.invoke(pixKey)
        }
    }

    companion object {
        fun create(
            parent: ViewGroup,
            onPixKeySelected: (pixKey: PixKey) -> Unit
        ): MyPixKeyViewHolder {
            val itemViewBinding = MyPixKeyItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return MyPixKeyViewHolder(itemViewBinding, onPixKeySelected)
        }
    }

}