package br.com.thiagooliveira.pixbank.utils

import android.view.View
import android.widget.ImageView
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.journeyapps.barcodescanner.BarcodeEncoder

fun generateQRCode(imageView: ImageView, pixKeyValue: String) {
    val multiFormatWriter = MultiFormatWriter()
    val barcodeEncoder = BarcodeEncoder()

    try {
        val bitMatrix = multiFormatWriter.encode(
            pixKeyValue, BarcodeFormat.QR_CODE,
            QRCODE_WIDTH,
            QRCODE_HEIGHT
        )
        val bitmap = barcodeEncoder.createBitmap(bitMatrix)

        imageView.apply {
            this.visibility = View.VISIBLE
            setImageBitmap(bitmap)
        }

    } catch (e: Exception) {
        e.printStackTrace()
    }
}

const val QRCODE_WIDTH = 300
const val QRCODE_HEIGHT = 300