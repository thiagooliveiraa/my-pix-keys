package br.com.thiagooliveira.pixbank.ui.mypixkeyslist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.thiagooliveira.domain.model.PixKey
import br.com.thiagooliveira.pixbank.repository.IPixKeyRepository
import br.com.thiagooliveira.pixbank.utils.*
import kotlinx.coroutines.launch

class MyPixKeysViewModel(
    private val pixKeyRepository: IPixKeyRepository
) : BaseViewModel() {

    private val _myKeysState = MutableLiveData<StateResponse<List<PixKey>>>()
    val myKeysState: LiveData<StateResponse<List<PixKey>>> = _myKeysState

    fun getMyPixKeys() {
        _myKeysState.value = StateLoading()
        launch {
            pixKeyRepository.getMyPixKeys()
                .onSuccess {
                    _myKeysState.value = StateSuccess(it)
                }.onError {
                    _myKeysState.value = StateError()
                }
        }
    }

    fun updateMyPixKeys(pixKey: PixKey) {
        val newPixKeysList = mutableListOf<PixKey>()
        _myKeysState.value?.data()?.apply {
            newPixKeysList.addAll(this)
        }
        newPixKeysList.add(pixKey)
        _myKeysState.value = StateSuccess(newPixKeysList)
    }
}
