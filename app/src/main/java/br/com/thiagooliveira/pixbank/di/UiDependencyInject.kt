package br.com.thiagooliveira.pixbank.di

import br.com.thiagooliveira.data.provider.IPixKeysProvider
import br.com.thiagooliveira.data.provider.PixKeysProvider
import br.com.thiagooliveira.pixbank.ui.mypixkeyslist.MyPixKeysViewModel
import br.com.thiagooliveira.pixbank.repository.IPixKeyRepository
import br.com.thiagooliveira.pixbank.repository.PixKeyRepositoryImpl
import br.com.thiagooliveira.pixbank.ui.registerpixkey.RegisterPixKeyViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object UiDependencyInject {

    private val uiModule = module(override = true) {
        single<IPixKeyRepository> { PixKeyRepositoryImpl(get()) }
        viewModel { MyPixKeysViewModel(get()) }
        viewModel { RegisterPixKeyViewModel(get()) }
    }

    private val dataModule = module(override = true) {
        single<IPixKeysProvider> { PixKeysProvider(get()) }
    }

    val moduleList = listOf(uiModule, dataModule).toTypedArray()
}