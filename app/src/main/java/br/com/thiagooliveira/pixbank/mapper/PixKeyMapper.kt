package br.com.thiagooliveira.pixbank.mapper

import br.com.thiagooliveira.data.response.PixKeyDTO
import br.com.thiagooliveira.domain.enum.PixKeyType
import br.com.thiagooliveira.domain.model.PixKey

fun PixKeyDTO.asDomainModel() = PixKey(
    id = id,
    value = value,
    type = PixKeyType.valueOf(type),
    bank = bank,
    bankImage = bankImage
)
