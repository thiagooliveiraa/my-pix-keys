package br.com.thiagooliveira.pixbank.utils

import android.content.Context
import android.content.Intent
import br.com.thiagooliveira.pixbank.R

fun sharePlainText(context: Context, text: String) {
    val sendIntent = Intent()
    sendIntent.action = Intent.ACTION_SEND
    sendIntent.putExtra(Intent.EXTRA_TEXT, text)
    sendIntent.type = SHARE_INTENT_TYPE
    Intent.createChooser(sendIntent, context.getString(R.string.share_by))
    context.startActivity(sendIntent)
}

private const val SHARE_INTENT_TYPE = "text/plain"