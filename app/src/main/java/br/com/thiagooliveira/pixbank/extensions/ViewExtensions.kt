package br.com.thiagooliveira.pixbank.extensions

import android.view.View

fun View.setVisible(isVisible: Boolean) = apply {
    this.visibility = if (isVisible) {
        View.VISIBLE
    } else {
        View.GONE
    }
}