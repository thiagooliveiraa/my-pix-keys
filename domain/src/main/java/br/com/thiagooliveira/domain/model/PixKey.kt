package br.com.thiagooliveira.domain.model

import br.com.thiagooliveira.domain.enum.PixKeyType
import java.io.Serializable

data class PixKey(
    val id: String? = null,
    var value: String? = null,
    var type: PixKeyType? = null,
    val bank: String? = DEFAULT_BANK,
    val bankImage: String? = DEFAULT_BANK_IMAGE
) : Serializable {
    companion object {
        const val DEFAULT_BANK = "PicPay"
        const val DEFAULT_BANK_IMAGE =
            "https://portalvhdshl0fsz1rywfcp.blob.core.windows.net/instituicoes-bancarias-logo/picpay-1.png"
    }
}