package br.com.thiagooliveira.domain.enum

enum class PixKeyType {
    CPF,
    PHONE,
    EMAIL,
    RANDOM;
}