package br.com.thiagooliveira.data

import br.com.thiagooliveira.data.provider.IPixKeysProvider
import br.com.thiagooliveira.data.provider.PixKeysProvider
import br.com.thiagooliveira.data.response.PixKeyDTO
import br.com.thiagooliveira.data.restclient.PixKeyRestClient
import br.com.thiagooliveira.data.retrofitutils.RequestResponse
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

class PixKeyProviderTest {

    @Mock
    lateinit var pixKeyRestClient: PixKeyRestClient

    @Mock
    lateinit var pixKeysProvider: IPixKeysProvider

    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
        pixKeysProvider = PixKeysProvider(pixKeyRestClient)
    }

    @Test
    fun getMyPixKeysWithSuccess(){
        runBlocking {
            Mockito.`when`(pixKeyRestClient.getMyPixKeys()).thenReturn(Response.success(pixKeysMockDTO))
            val pixKeys = pixKeysProvider.getMyPixKeys()
            Assert.assertEquals((pixKeys as RequestResponse.Success).value, pixKeysMockDTO)
        }
    }

    @Test
    fun getMyPixKeysWithError(){
        runBlocking {
            Mockito.`when`(pixKeyRestClient.getMyPixKeys()).thenReturn(Response.error(ERROR_CODE, ResponseBody.create(
                MediaType.parse("application/json"),
                ""
            )))
            val pixKeys = pixKeysProvider.getMyPixKeys()
            Assert.assertEquals((pixKeys as RequestResponse.Error).httpCode, ERROR_CODE)
        }
    }

    companion object {
        const val ERROR_CODE = 500

        val pixKeysMockDTO = listOf(
            PixKeyDTO(
                "1",
                "123.456.789-10",
                "CPF",
                "Picpay",
                ""
            ),
            PixKeyDTO(
                "2",
                "123.456.789-20",
                "CPF",
                "Picpay",
                ""
            )
        )
    }

}