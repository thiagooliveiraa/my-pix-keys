package br.com.thiagooliveira.data.restclient

import br.com.thiagooliveira.data.response.PixKeyDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST

interface PixKeyRestClient {

    @GET("75e34a2f-e7ec-41b4-9e37-bbf301338515")
    suspend fun getMyPixKeys(): Response<List<PixKeyDTO>>

    @POST("4e20edcf-059a-458c-8b4b-889eeb628ffa")
    suspend fun registerPixKeys(): Response<Unit>

}