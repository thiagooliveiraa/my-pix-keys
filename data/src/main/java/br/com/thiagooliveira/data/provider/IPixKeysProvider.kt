package br.com.thiagooliveira.data.provider

import br.com.thiagooliveira.data.response.PixKeyDTO
import br.com.thiagooliveira.data.retrofitutils.RequestResponse

interface IPixKeysProvider {
    suspend fun getMyPixKeys(): RequestResponse<List<PixKeyDTO>>
    suspend fun registerPixKey(): RequestResponse<Unit>
}
