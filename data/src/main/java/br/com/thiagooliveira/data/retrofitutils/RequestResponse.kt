package br.com.thiagooliveira.data.retrofitutils

import retrofit2.Response

sealed class RequestResponse<T> {

    class Success<T>(private val response: Response<T>) : RequestResponse<T>() {
        val value get() = response.body()
    }

    class Error<T>(val response: Response<T>) : RequestResponse<T>() {
        val httpCode get() = response.code()
        val body get() = response.body()
        val errorBody get() = response.errorBody()
    }
}