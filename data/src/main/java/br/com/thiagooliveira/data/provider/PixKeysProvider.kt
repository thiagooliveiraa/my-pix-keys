package br.com.thiagooliveira.data.provider

import br.com.thiagooliveira.data.response.PixKeyDTO
import br.com.thiagooliveira.data.restclient.PixKeyRestClient
import br.com.thiagooliveira.data.retrofitutils.RequestResponse
import br.com.thiagooliveira.data.retrofitutils.formatResponse

class PixKeysProvider(
    private val pixKeyRestClient: PixKeyRestClient
): IPixKeysProvider {

    override suspend fun getMyPixKeys(): RequestResponse<List<PixKeyDTO>> {
        return pixKeyRestClient.getMyPixKeys().formatResponse()
    }

    override suspend fun registerPixKey(): RequestResponse<Unit> {
        return pixKeyRestClient.registerPixKeys().formatResponse()
    }
}