package br.com.thiagooliveira.data.di

import br.com.thiagooliveira.data.restclient.PixKeyRestClient
import com.google.gson.*
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

object DataDependencyInject {

    val remoteDataModule = module {
        single { provideOkHttpClient() }
        single { provideGson() }

        factory {
            pixKeysClient(get(), get()).create(PixKeyRestClient::class.java)
        }
    }

    private fun pixKeysClient(
        okHttpClient: OkHttpClient,
        gson: Gson
    ) = Retrofit.Builder()
        .baseUrl("https://run.mocky.io/v3/")
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(okHttpClient)
        .build()

    private fun provideOkHttpClient(): OkHttpClient {
        val okHttp = OkHttpClient.Builder()
                .connectTimeout(CONNECTION_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
                .writeTimeout(CONNECTION_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
                .readTimeout(CONNECTION_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        okHttp.addInterceptor(loggingInterceptor)

        return okHttp.build()
    }

    private fun provideGson() = GsonBuilder()
            .registerTypeAdapter(Date::class.java, JsonDeserializer { json, _, _ ->
                Date(json.asJsonPrimitive.asLong)
            })
            .registerTypeAdapter(Date::class.java, JsonSerializer<Date> { date, _, _ ->
                JsonPrimitive(date.time)
            })
            .create()

    private const val CONNECTION_TIMEOUT: Int = 120000
}