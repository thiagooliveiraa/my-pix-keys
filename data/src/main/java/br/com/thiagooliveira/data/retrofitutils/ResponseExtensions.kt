package br.com.thiagooliveira.data.retrofitutils

import retrofit2.Response

fun <T> Response<T>.formatResponse(): RequestResponse<T> {
    return this.let {
        if(it.isSuccessful){
            RequestResponse.Success(it)
        } else {
            RequestResponse.Error(it)
        }
    }
}