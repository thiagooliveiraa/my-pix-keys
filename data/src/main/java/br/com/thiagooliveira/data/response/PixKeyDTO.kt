package br.com.thiagooliveira.data.response

import com.google.gson.annotations.SerializedName

data class PixKeyDTO(
    @SerializedName("id") val id: String,
    @SerializedName("value") val value: String,
    @SerializedName("type") val type: String,
    @SerializedName("bank") val bank: String,
    @SerializedName("bankImage") val bankImage: String
)
